import React from "react";
import Img from "../../../components/Image/Image";
import Svg from "../../../components/Svg/Svg";
import { SvgProps } from "../../../components/Svg/types";

interface LogoProps extends SvgProps {
  isDark: boolean;
}

const Logo: React.FC<LogoProps> = ({ isDark, ...props }) => {
  const textColor = isDark ? "#FFFFFF" : "#000000";
    return (
        <img src="/images/HeadLogo.png" style={{ height: '30px' }} alt="GemSwap" />
    )
};

export default React.memo(Logo, (prev, next) => prev.isDark === next.isDark);
